<?php

return [
    'client_id' => env('PAYTRAIL_MERCHANT_ID'),
    'client_secret' => env('PAYTRAIL_MERCHANT_SECRET'),
];
