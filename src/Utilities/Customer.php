<?php

namespace SirMathays\Paytrail\Utilities;

use Paytrail\E2Module\Customer as E2ModuleCustomer;

/**
 * @property string|null $firstname
 * @method static setFirstname(string|null $value = null)
 * @property string|null $lastname
 * @method static setLastname(string|null $value = null)
 * @property string|null $email
 * @method static setEmail(string|null $value = null)
 * @property string|null $addrStreet
 * @method static setAddrStreet(string|null $value = null)
 * @property string|null $addrPostalCode
 * @method static setAddrPostalCode(string|null $value = null)
 * @property string|null $addrTown
 * @method static setAddrTown(string|null $value = null)
 * @property string|null $addrCountry
 * @method static setAddrCountry(string|null $value = null)
 * @property string|null $phone
 * @method static setPhone(string|null $value = null)
 * @property string|null $companyName
 * @method static setCompanyName(string|null $value = null)
 */
class Customer extends Frame
{
    protected const ATTRIBUTE_DEFINITIONS = [
        'firstname' => 'PAYER_PERSON_FIRSTNAME',
        'lastname' => 'PAYER_PERSON_LASTNAME',
        'email' => 'PAYER_PERSON_EMAIL',
        'addrStreet' => 'PAYER_PERSON_ADDR_STREET',
        'addrPostalCode' => 'PAYER_PERSON_ADDR_POSTAL_CODE',
        'addrTown' => 'PAYER_PERSON_ADDR_TOWN',
        'addrCountry' => 'PAYER_PERSON_ADDR_COUNTRY',
        'phone' => 'PAYER_PERSON_PHONE',
        'companyName' => 'PAYER_COMPANY_NAME',
    ];

    /**
     * Convert into E2Module Customer.
     *
     * @return mixed
     */
    public function convert()
    {
        return E2ModuleCustomer::create($this->getConvertedAttributes());
    }
}
