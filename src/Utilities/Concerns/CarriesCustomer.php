<?php

namespace SirMathays\Paytrail\Utilities\Concerns;

use SirMathays\Paytrail\Utilities\Customer;

trait CarriesCustomer
{
    protected ?Customer $customer = null;

    /**
     * Get the customer set for the payment.
     *
     * @return Customer|null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * Set customer for the payment.
     *
     * @param array|Customer $customer
     * @return $this
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer instanceof Customer
            ? $customer
            : new Customer($customer);

        return $this;
    }

    /**
     * Remove the customer from the payment.
     *
     * @return $this
     */
    public function removeCustomer(): self
    {
        $this->customer = null;
        return $this;
    }
}
