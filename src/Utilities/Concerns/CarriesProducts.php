<?php

namespace SirMathays\Paytrail\Utilities\Concerns;

use Illuminate\Support\Collection;
use SirMathays\Paytrail\Utilities\Product;

trait CarriesProducts
{
    protected Collection $products;

    /**
     * Get the products.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * Remove product from the collection.
     *
     * @param int $index
     * @return $this
     */
    public function removeProduct($index): self
    {
        $this->products->offsetUnset($index);

        return $this;
    }

    /**
     * Add products to the payment.
     *
     * @param mixed $products
     * @return $this
     */
    public function addProducts($products)
    {
        (new Collection($products))->each(
            fn ($product) => $this->addProduct($product)
        );

        return $this;
    }

    /**
     * Add a single product to the payment.
     *
     * @param mixed $product
     * @return $this
     */
    public function addProduct($product)
    {
        $this->products->push(
            $product instanceof Product
                ? $product
                : new Product($product)
        );

        return $this;
    }

    /**
     * Initialize trait for the Frame instance.
     *
     * @return void
     */
    protected function initializeCarriesProducts(): void
    {
        $this->products = new Collection();
    }
}
