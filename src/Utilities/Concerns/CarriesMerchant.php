<?php

namespace SirMathays\Paytrail\Utilities\Concerns;

use Paytrail\E2Module\Merchant;

trait CarriesMerchant
{
    protected ?Merchant $merchant = null;

    /**
     * Get the merchant set for the payment.
     *
     * @return \Paytrail\E2Module\Merchant|null
     */
    public function getMerchant(): ?Merchant
    {
        return $this->merchant;
    }

    /**
     * Set merchant for the payment.
     *
     * @param \Paytrail\E2Module\Merchant $merchant
     * @return $this
     */
    public function setMerchant(Merchant $merchant)
    {
        $this->merchant = $merchant;
        return $this;
    }

    /**
     * Remove the merchant from the payment.
     *
     * @return $this
     */
    public function removeMerchant(): self
    {
        $this->merchant = null;
        return $this;
    }
}
