<?php

namespace SirMathays\Paytrail\Utilities;

use BadMethodCallException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Paytrail\E2Module\E2Payment;
use Paytrail\E2Module\Merchant;

/**
 * @property mixed $orderNumber
 * @method static setOrderNumber($value = null)
 * @property string|null $locale
 * @method static setLocale($value = null)
 * @property double|null $amount
 * @method static setAmount($value = null)
 * @property string|null $currency
 * @method static setCurrency($value = null)
 * @property string|null $paymentMethods
 * @method static setPaymentMethods($value = null)
 * @property string|null $paramsIn
 * @method static setParamsIn($value = null)
 * @property string|null $paramsOut
 * @method static setParamsOut($value = null)
 * @property mixed|null $urlSuccess
 * @method static setUrlSuccess($value = null)
 * @property mixed|null $urlCancel
 * @method static setUrlCancel($value = null)
 * @property mixed|null $urlNotify
 * @method static setUrlNotify($value = null)
 * @property mixed|null $msgUiMerchantPanel
 * @method static setMsgUiMerchantPanel($value = null)
 * @property mixed|null $msgSettlementPayer
 * @method static setMsgSettlementPayer($value = null)
 * @property mixed|null $msgUiPaymentMethod
 * @method static setMsgUiPaymentMethod($value = null)
 */
class Payment extends Frame
{
    use Concerns\CarriesMerchant,
        Concerns\CarriesProducts,
        Concerns\CarriesCustomer;

    protected const ATTRIBUTE_DEFINITIONS = [
        'amount' => 'AMOUNT',
        'currency' => 'CURRENCY',
        'locale' => 'LOCALE',
        'orderNumber' => 'ORDER_NUMBER',
        'paramsIn' => 'PARAMS_IN',
        'paramsOut' => 'PARAMS_OUT',
        'paymentMethods' => 'PAYMENT_METHODS',
        'urlCancel' => 'URL_CANCEL',
        'urlNotify' => 'URL_NOTIFY',
        'urlSuccess' => 'URL_SUCCESS',
        'msgUiMerchantPanel' => 'MSG_UI_MERCHANT_PANEL',
        'msgSettlementPayer' => 'MSG_SETTLEMENT_PAYER',
        'msgUiPaymentMethod' => 'MSG_UI_PAYMENT_METHOD',
    ];

    /**
     * Convert into E2Payment instance.
     *
     * @return mixed
     */
    public function convert()
    {
        throw_unless(
            $this->merchant instanceof Merchant,
            new BadMethodCallException("Payment must contain a Merchant before conversion.")
        );

        $payment = new E2Payment($this->merchant);

        if (!is_null($this->customer)) {
            $payment->addCustomer($this->customer->convert());
        }

        if (!is_null($this->amount)) {
            $payment->addAmount($this->amount);
        }

        if ($this->products->isNotEmpty()) {
            $products = $this->products
                ->map(fn (Product $product) => $product->convert())
                ->toArray();

            $payment->addProducts($products);
        }

        $paymentData = (new Collection($this->getConvertedAttributes()))
            ->except('ORDER_NUMBER')
            ->filter(fn ($row) => !is_null($row))
            ->toArray();

        return $payment->createPayment($this->orderNumber, $paymentData);
    }
}
