<?php

namespace SirMathays\Paytrail\Utilities;

use BadMethodCallException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use RuntimeException;

abstract class Frame
{
    protected const ATTRIBUTE_DEFINITIONS = [];

    protected const DEFAULTS = [];

    protected array $attributes = [];

    /**
     * The Customer constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->initializeTraits();

        $this->fill(array_merge(static::DEFAULTS, $attributes));
    }

    /**
     * Fill attributes for the customer.
     *
     * @param array $attributes
     * @return $this
     */
    public function fill(array $attributes = []): self
    {
        (new Collection($attributes))
            ->filter(fn ($value, $key) => static::validAttribute($key))
            ->each(fn ($value, $key) => $this->{$key} = $value);

        return $this;
    }

    /**
     * Get attributes.
     *
     * @return array
     */
    public function getAttributes(): array
    {
        return (new Collection(static::ATTRIBUTE_DEFINITIONS))
            ->keys()
            ->mapWithKeys(fn ($key) => [$key => $this->{$key}])
            ->toArray();
    }

    /**
     * Get an attribute
     *
     * @param string $name
     * @return mixed
     * @throws \RuntimeException
     */
    public function getAttribute($name)
    {
        if (static::getMutatorExists($name)) {
            return $this->callGetMutator($name);
        }

        if (static::validAttribute($name)) {
            return $this->attributes[$name] ?? null;
        }

        throw new RuntimeException("Unsupported attribute given.");
    }

    /**
     * Set an attribute
     *
     * @param string $name
     * @param mixed $value
     * @return void
     * @throws \RuntimeException
     */
    public function setAttribute($name, $value)
    {
        if (static::setMutatorExists($name)) {
            return $this->callSetMutator($name, $value);
        }

        else if (static::validAttribute($name)) {
            return $this->attributes[$name] = $value;
        }

        throw new RuntimeException("Unsupported attribute given.");
    }

    /**
     * Get attributes in raw format.
     *
     * @return array
     */
    public function getRawAttributes(): array
    {
        return (new Collection(static::ATTRIBUTE_DEFINITIONS))
            ->keys()
            ->mapWithKeys(fn ($key) => [$key => $this->attributes[$key] ?? null])
            ->toArray();
    }

    /**
     * Initialize traits for the instance.
     *
     * @return void
     */
    protected function initializeTraits(): void
    {
        foreach (class_uses_recursive(static::class) as $trait) {
            if (method_exists($this, $methodName = 'initialize' . class_basename($trait))) {
                $this->{$methodName}();
            }
        }
    }

    /**
     * Get the attributes of the customer.
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        try {
            return $this->getAttribute($name);
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * Set the attributes of the customer.
     *
     * @param string $name
     * @param string $value
     */
    public function __set($name, $value)
    {
        try {
            $this->setAttribute($name, $value);
        } catch (\Throwable $th) {
            return;
        }
    }

    /**
     * Call the
     *
     * @param string $name
     * @param array $arguments
     * @return void
     */
    public function __call($name, $arguments)
    {
        try {
            if (Str::startsWith($name, 'set')) {
                $attribute = Str::camel(Str::after($name, 'set'));
                $this->setAttribute($attribute, $arguments[0] ?? null);

                return $this;
            }
        } catch (\Throwable $th) {
            throw new BadMethodCallException(sprintf(
                'Call to undefined method %s::%s()', static::class, $name
            ));
        }
    }

    /**
     * Validate given attribute name is supported.
     *
     * @param string $name
     * @return bool
     */
    protected static function validAttribute($name): bool
    {
        return array_key_exists($name, static::ATTRIBUTE_DEFINITIONS);
    }

    /**
     * Validate the existence of a get mutator.
     *
     * @param string $name
     * @return bool
     */
    protected static function getMutatorExists($name): bool
    {
        return method_exists(static::class, 'get' . Str::studly($name) . 'Attribute');
    }

    /**
     * Call given get mutator.
     *
     * @param string $name
     * @return void
     */
    protected function callGetMutator($name)
    {
        return $this->{'get' . Str::studly($name) . 'Attribute'}();
    }

    /**
     * Validate the existence of a set mutator.
     *
     * @param string $name
     * @return bool
     */
    protected static function setMutatorExists($name): bool
    {
        return method_exists(static::class, 'set' . Str::studly($name) . 'Attribute');
    }

    /**
     * Call given get mutator.
     *
     * @param string $name
     * @return void
     */
    protected function callSetMutator($name, $value)
    {
        return $this->{'set' . Str::studly($name) . 'Attribute'}($value);
    }

    /**
     * Return attributes converted to match E2Module attributes.
     *
     * @return array
     */
    public function getConvertedAttributes(): array
    {
        return (new Collection($this->getRawAttributes()))
            ->mapWithKeys(fn ($value, $key) => [static::ATTRIBUTE_DEFINITIONS[$key] => $value])
            ->toArray();
    }

    /**
     * Convert into E2Module instance.
     *
     * @return mixed
     */
    abstract public function convert();
}
