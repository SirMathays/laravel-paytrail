<?php

namespace SirMathays\Paytrail\Utilities;

use InvalidArgumentException;
use Paytrail\E2Module\Product as E2ModuleProduct;

/**
 * @property int $id
 * @method static setId($value = null)
 * @property string $title
 * @method static setTitle($value = null)
 * @property double $unitPrice
 * @method static setUnitPrice($value = null)
 * @property int $vatPercent
 * @method static setVatPercent($value = null)
 * @property int $quantity
 * @method static setQuantity($value = null)
 * @property string $type
 * @method static setType($value = null)
 * @property int $discountPercent
 * @method static setDiscountPercent($value = null)
 */
class Product extends Frame
{
    protected const ATTRIBUTE_DEFINITIONS = [
        'discountPercent' => 'ITEM_DISCOUNT_PERCENT',
        'id' => 'ITEM_ID',
        'quantity' => 'ITEM_QUANTITY',
        'title' => 'ITEM_TITLE',
        'type' => 'ITEM_TYPE',
        'unitPrice' => 'ITEM_UNIT_PRICE',
        'vatPercent' => 'ITEM_VAT_PERCENT',
    ];

    protected const DEFAULTS = [
        'vatPercent' => E2ModuleProduct::DEFAULT_VAT,
        'quantity' => 1,
        'type' => E2ModuleProduct::TYPE_NORMAL
    ];

    protected const PRODUCT_TYPES = [
        'normal' => E2ModuleProduct::TYPE_NORMAL,
        'shipmentCost' => E2ModuleProduct::TYPE_SHIPMENT_COST,
        'handlingCost' => E2ModuleProduct::TYPE_HANDLING_COST,
    ];

    /**
     * Product type attribute getter.
     *
     * @return int
     */
    public function getTypeAttribute(): string
    {
        return array_flip(static::PRODUCT_TYPES)[$this->attributes['type']];
    }

    /**
     * Product type attribute setter.
     *
     * @return int
     */
    public function setTypeAttribute(string $value)
    {
        if (array_key_exists($value, static::PRODUCT_TYPES)) {
            $this->attributes['type'] = static::PRODUCT_TYPES[$value];
        } else if (in_array($value, static::PRODUCT_TYPES)) {
            $this->attributes['type'] = $value;
        } else {
            throw new InvalidArgumentException("Unsupported product type given.");
        }
    }

    /**
     * Convert into E2Module product instance.
     *
     * @return void
     */
    public function convert()
    {
        return E2ModuleProduct::create($this->getConvertedAttributes());
    }
}
