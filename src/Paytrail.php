<?php

namespace SirMathays\Paytrail;

use SirMathays\Paytrail\Utilities\Customer;
use Paytrail\E2Module\Authcode;
use Paytrail\E2Module\Customer as E2ModuleCustomer;
use Paytrail\E2Module\E2Payment;
use Paytrail\E2Module\Merchant;
use SirMathays\Paytrail\Utilities\Payment;

class Paytrail
{
    protected PaytrailConfig $config;
    protected Merchant $merchant;

    /**
     * The Paytrail constructor.
     *
     * @param \Paytrail\E2Module\Merchant $merchant
     * @param PaytrailConfig $config
     */
    public function __construct(Merchant $merchant, PaytrailConfig $config)
    {
        $this->config = $config;
        $this->merchant = $merchant;
    }

    /**
     * Return the instance of the config.
     *
     * @return PaytrailConfig
     */
    public function getConfig(): PaytrailConfig
    {
        return $this->config;
    }

    /**
     * Return the instance of the config.
     *
     * @return \Paytrail\E2Module\Merchant
     */
    public function getMerchant(): Merchant
    {
        return $this->merchant;
    }

    /**
     * Make a new merchant instance.
     *
     * @param mixed $id
     * @param string $secret
     * @return \Paytrail\E2Module\Merchant
     */
    public function makeMerchant($id, $secret): Merchant
    {
        return Merchant::create($id, $secret);
    }

    /**
     * Init payment utility.
     *
     * @param array $attributes
     * @param \Paytrail\E2Module\Merchant|null $merchant
     * @return \SirMathays\Paytrail\Utilities\Payment
     */
    public function initPayment(array $attributes = [], ?Merchant $merchant = null): Payment
    {
        return (new Payment($attributes))->setMerchant($merchant ?: $this->merchant);
    }

    /**
     * Calculate expected return authcode.
     *
     * @param array $returnParameters
     * @param \Paytrail\E2Module\Merchant|null $merchant
     * @return string
     */
    public function calculateReturnAuthCode(array $returnParameters, ?Merchant $merchant = null): string
    {
        return Authcode::calculateReturnAuthCode($returnParameters, $merchant ?: $this->merchant);
    }
}
