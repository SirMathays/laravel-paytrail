<?php

namespace SirMathays\Paytrail;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Paytrail\E2Module\Merchant as PaytrailMerchant;

class PaytrailServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->bind(PaytrailConfig::class, function () {
            return new PaytrailConfig($this->app['config']->get('services.paytrail', []));
        });

        $this->app->singleton(PaytrailMerchant::class, function (Application $app) {
            /** @var PaytrailConfig $config */
            $config = $app->make(PaytrailConfig::class);

            return PaytrailMerchant::create($config->getClientId(), $config->getClientSecret());
        });

        $this->app->singleton(Paytrail::class, function (Application $app) {
            return new Paytrail(
                $app->make(PaytrailMerchant::class),
                $app->make(PaytrailConfig::class)
            );
        });
    }
}
