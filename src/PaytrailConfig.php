<?php

namespace SirMathays\Paytrail;

use Illuminate\Support\Arr;

class PaytrailConfig
{
    protected array $config;

    /**
     * The PaytrailConfig constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function getClientId(): ?string
    {
        return Arr::get($this->config, 'client_id');
    }

    public function getClientSecret(): ?string
    {
        return Arr::get($this->config, 'client_secret');
    }
}
