<?php

namespace SirMathays\Paytrail\Facades;

use SirMathays\Paytrail\Paytrail as BasePaytrail;
use Illuminate\Support\Facades\Facade;

/**
 * @method static \Paytrail\E2Module\E2Payment makePayment(\Paytrail\E2Module\Merchant|null $merchant)
 * @method static \Paytrail\E2Module\Merchant makeMerchant($id, $secret)
 */
class Paytrail extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return BasePaytrail::class;
    }
}
